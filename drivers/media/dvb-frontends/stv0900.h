/*
 * stv0900.h
 *
 * Driver for ST STV0900 satellite demodulator IC.
 *
 * Copyright (C) ST Microelectronics.
 * Copyright (C) 2009 NetUP Inc.
 * Copyright (C) 2009 Igor M. Liplianin <liplianin@netup.ru>
 * Copyright (C) 2009-2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef STV0900_H
#define STV0900_H

#include <linux/kconfig.h>
#include <linux/dvb/frontend.h>
#include "dvb_frontend.h"

enum stv0900_tuner_type {
	STV0900_TUNER_STB6100,
	STV0900_TUNER_STV6110
};

struct stv0900_reg {
	u16 addr;
	u8  val;
};

struct stv0900_config {
	u32	xtal;
	u32	path1_clk;
	u32	path2_clk;
	u32	path3_clk;
	u8	demod_address;
	u8	demod_mode;
	u8	clkmode;/* 0 for CLKI,  2 for XTALI */
	u8	diseqc_mode;
	u8	path1_mode;
	u8	path2_mode;
	u8	path3_mode;
	u8	tun1_maddress;/* 0, 1, 2, 3 for 0xc0, 0xc2, 0xc4, 0xc6 */
	u8	tun2_maddress;
	u8	tun1_auto;/* 0 for sw controlled, 1 for demod controlled */
	u8	tun2_auto;
	u8	tun1_type;/* STV0900_TUNER_STB6100 or STV0900_TUNER_STV6110 */
	u8	tun2_type;
	u8	tun1_clkdiv;/* 1, 2, 4, 8 */
	u8	tun2_clkdiv;

	struct stv0900_reg *ts_config_regs;

	/* Set device param to start dma */
	int (*set_ts_params)(struct dvb_frontend *fe, int is_punctured);
	/* Hook for Lock LED */
	void (*set_lock_led)(struct dvb_frontend *fe, int offon);
};

#if IS_REACHABLE(CONFIG_DVB_STV0900)
extern struct dvb_frontend *stv0900_attach(const struct stv0900_config *config,
					struct i2c_adapter *i2c, int demod);
#else
static inline struct dvb_frontend *stv0900_attach(const struct stv0900_config *config,
					struct i2c_adapter *i2c, int demod)
{
	printk(KERN_WARNING "%s: driver disabled by Kconfig\n", __func__);
	return NULL;
}
#endif

#endif

