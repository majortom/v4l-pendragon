/*
 * STV6120 Silicon tuner driver
 *
 * Copyright (C)      Chris Lee <updatelee@gmail.com>
 * Copyright (C) 2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __STV6120_H__
#define __STV6120_H__

#include <linux/kconfig.h>
#include <linux/dvb/frontend.h>
#include "dvb_frontend.h"

enum stv6120_tuner {
	STV6120_TUNER1 = 0,
	STV6120_TUNER2,
};

enum stv6120_rfsel {
	STV6120_RFA =  0,
	STV6120_RFB,
	STV6120_RFC,
	STV6120_RFD,
	STV6120_RF_NONE,
};

struct stv6120_config {
	u32					refclk;	//	reference clock in Hz
	u8					adr;
	u8					odiv;	//	output clock divisor
	enum stv6120_tuner	tuner;
	enum stv6120_rfsel	rfsel;
};

#if IS_REACHABLE(CONFIG_DVB_STV6120)
extern struct dvb_frontend *stv6120_attach(struct dvb_frontend *fe, struct i2c_adapter *i2c, const struct stv6120_config *config);
#else
static inline struct dvb_frontend *stv6120_attach(struct dvb_frontend *fe, struct i2c_adapter *i2c, const struct stv6120_config *config)
{
	pr_info("%s: driver disabled by Kconfig\n", __func__);
	return NULL;
}
#endif /* CONFIG_DVB_STV6120 */
#endif /* __STV6120_H__ */
