/*
 * Driver for the STM STV0910 DVB-S/S2 demodulator.
 *
 * Copyright (C) 2014-2015 Ralph Metzler <rjkm@metzlerbros.de>
 *                         Marcus Metzler <mocm@metzlerbros.de>
 *                         developed for Digital Devices GmbH
 * Copyright (C) 2015-2016 Chris Lee <updatelee@gmail.com>
 * Copyright (C) 2016      Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 only, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA
 * Or, point your browser to http://www.gnu.org/copyleft/gpl.html
 */

#ifndef _STV0910_H_
#define _STV0910_H_

#include <linux/kconfig.h>
#include <linux/dvb/frontend.h>
#include "dvb_frontend.h"

enum stv0910_tuner_type {
	STV0910_TUNER_STV6111,
	STV0910_TUNER_STV6120,
};

struct stv0910_init_pair {
	u16  reg;
	u8   val;
};

struct stv0910_init_table {
	const struct stv0910_init_table	*next;
	const struct stv0910_init_pair	*init;
	const int size;
};

#define STV0910_INIT_TABLE(nm, tbl, nxt)	static const struct stv0910_init_table nm = { \
												.next = nxt, \
												.init = tbl, \
												.size = sizeof(tbl) / sizeof(struct stv0910_init_pair) \
											}

struct stv0910_config {
	const struct stv0910_init_table	*init;
	enum stv0910_tuner_type			tunerType;
	
	u32		extclk;			//	in Hz
	u32		mclk;			//	in Hz
	u8		adr;
	u8		single;			//	if demod operated in single mode
	u8		demod;			//	0 -> P1, 1 -> P2
	u8		tunerI2C;		//	0 -> P1, 2 -> P2
	u8		tunerChips;		//	one dual tuner chip = 1, two tuner chips = 2
	u8		rptlvl;			//	I2C repeat level to tuner
	u8		serial;			//	transport stream serial output
	u8		disrxpin;		//	DiSEqC receiver pin
};

#if defined(CONFIG_DVB_STV0910) || \
	(defined(CONFIG_DVB_STV0910_MODULE) && defined(MODULE))

extern struct dvb_frontend *stv0910_attach(struct i2c_adapter *i2c, const struct stv0910_config *config);
#else

static inline struct dvb_frontend *stv0910_attach(struct i2c_adapter *i2c, const struct stv0910_config *config)
{
	pr_warn("%s: driver disabled by Kconfig\n", __func__);
	return NULL;
}

#endif

#endif
