/*
 * stv0900_priv.h
 *
 * Driver for ST STV0900 satellite demodulator IC.
 *
 * Copyright (C) ST Microelectronics.
 * Copyright (C) 2009 NetUP Inc.
 * Copyright (C) 2009 Igor M. Liplianin <liplianin@netup.ru>
 * Copyright (C) 2009-2016 Pendragon Sound
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef STV0900_PRIV_H
#define STV0900_PRIV_H

#include <linux/i2c.h>
#include <linux/mutex.h>

#define ABS(X) ((X) >= 0 ? (X) : -(X))
#define INRANGE(X, Y, Z) ((((X) <= (Y)) && ((Y) <= (Z))) \
		|| (((Z) <= (Y)) && ((Y) <= (X))) ? 1 : 0)

#ifndef MAKEWORD
#define MAKEWORD(X, Y) (((X) << 8) + (Y))
#endif

#define LSB(X) (((X) & 0xff))
#define MSB(Y) (((Y) >> 8) & 0xff)

#define dprintk(args...) do { if (stvdebug) printk(KERN_DEBUG args); } while (0)

#define usleep_release(a)	if (demod->mutex_count) mutex_unlock(&demod->chip->demod_mutex);	\
							dvb_frontend_nsleep_release(&demod->frontend, (a) * 1000LL);													\
							if (demod->mutex_count) mutex_lock(&demod->chip->demod_mutex)
#define usleep(a)			nsleep((a) * 1000LL)

#define STV0900_BLIND_SEARCH_AGC2_TH		700
#define STV0900_BLIND_SEARCH_AGC2_TH_CUT30	1400
#define IQPOWER_THRESHOLD					30

/* One point of the lookup table */
struct stv0900_point {
	s32 realval;/* real value */
	s32 regval;/* binary value */
};

enum stv0900_error {
	STV0900_NO_ERROR = 0,
	STV0900_INVALID_HANDLE,
	STV0900_BAD_PARAMETER,
	STV0900_I2C_ERROR,
	STV0900_SEARCH_FAILED,
};

enum stv0900_clock_type {
	STV0900_USE_REGISTERS_DEFAULT,
	STV0900_SERIAL_PUNCT_CLOCK,/*Serial punctured clock */
	STV0900_SERIAL_CONT_CLOCK,/*Serial continues clock */
	STV0900_PARALLEL_PUNCT_CLOCK,/*Parallel punctured clock */
	STV0900_DVBCI_CLOCK/*Parallel continues clock : DVBCI */
};

enum stv0900_search_state {
	STV0900_SEARCH = 0,
	STV0900_PLH_DETECTED,
	STV0900_DVBS2_FOUND,
	STV0900_DVBS_FOUND
};

enum stv0900_ldpc_state {
	STV0900_PATH1_OFF_PATH2_OFF = 0,
	STV0900_PATH1_ON_PATH2_OFF = 1,
	STV0900_PATH1_OFF_PATH2_ON = 2,
	STV0900_PATH1_ON_PATH2_ON = 3
};

enum stv0900_signal_type {
	STV0900_NOAGC1 = 0,
	STV0900_AGC1OK,
	STV0900_NOTIMING,
	STV0900_ANALOGCARRIER,
	STV0900_TIMINGOK,
	STV0900_NOAGC2,
	STV0900_AGC2OK,
	STV0900_NOCARRIER,
	STV0900_CARRIEROK,
	STV0900_NODATA,
	STV0900_DATAOK,
	STV0900_OUTOFRANGE,
	STV0900_RANGEOK,
};

enum stv0900_demod_num {
	STV0900_DEMOD_1,
	STV0900_DEMOD_2
};

enum stv0900_standard {
	STV0900_DSS_STANDARD,
	STV0900_DVBS1_STANDARD,
	STV0900_DVBS2_STANDARD,
	STV0900_UNKNOWN_STANDARD,
	STV0900_AUTO_STANDARD,
};

enum stv0900_algo {
	STV0900_BLIND_SEARCH,/* offset freq and SR are Unknown */
	STV0900_COLD_START,/* only the SR is known */
	STV0900_WARM_START,/* offset freq and SR are known */
	STV0900_BLIND_SCAN_AEP,
};

enum stv0900_modulation {
	STV0900_QPSK,
	STV0900_8PSK,
	STV0900_16APSK,
	STV0900_32APSK,
	STV0900_UNKNOWN
};

enum stv0900_modcode {
	STV0900_DUMMY_PLF,
	STV0900_QPSK_14,
	STV0900_QPSK_13,
	STV0900_QPSK_25,
	STV0900_QPSK_12,
	STV0900_QPSK_35,
	STV0900_QPSK_23,
	STV0900_QPSK_34,
	STV0900_QPSK_45,
	STV0900_QPSK_56,
	STV0900_QPSK_89,
	STV0900_QPSK_910,
	STV0900_8PSK_35,
	STV0900_8PSK_23,
	STV0900_8PSK_34,
	STV0900_8PSK_56,
	STV0900_8PSK_89,
	STV0900_8PSK_910,
	STV0900_16APSK_23,
	STV0900_16APSK_34,
	STV0900_16APSK_45,
	STV0900_16APSK_56,
	STV0900_16APSK_89,
	STV0900_16APSK_910,
	STV0900_32APSK_34,
	STV0900_32APSK_45,
	STV0900_32APSK_56,
	STV0900_32APSK_89,
	STV0900_32APSK_910,
	STV0900_MODCODE_UNKNOWN
};

enum stv0900_fec {/*DVBS1, DSS and turbo code puncture rate*/
	STV0900_FEC_1_2 = 0,
	STV0900_FEC_2_3,
	STV0900_FEC_3_4,
	STV0900_FEC_4_5,/*for turbo code only*/
	STV0900_FEC_5_6,
	STV0900_FEC_6_7,/*for DSS only */
	STV0900_FEC_7_8,
	STV0900_FEC_8_9,/*for turbo code only*/
	STV0900_FEC_UNKNOWN
};

enum stv0900_frame_length {
	STV0900_LONG_FRAME,
	STV0900_SHORT_FRAME
};

enum stv0900_pilot {
	STV0900_PILOTS_OFF,
	STV0900_PILOTS_ON
};

enum stv0900_rolloff {
	STV0900_35,
	STV0900_25,
	STV0900_20
};

enum stv0900_search_iq {
	STV0900_IQ_AUTO,
	STV0900_IQ_AUTO_NORMAL_FIRST,
	STV0900_IQ_FORCE_NORMAL,
	STV0900_IQ_FORCE_SWAPPED
};

enum stv0900_iq_inversion {
	STV0900_IQ_NORMAL,
	STV0900_IQ_SWAPPED
};

enum stv0900_diseqc_mode {
	STV0900_22KHZ_Continues = 0,
	STV0900_DISEQC_2_3_PWM = 2,
	STV0900_DISEQC_3_3_PWM = 3,
	STV0900_DISEQC_2_3_ENVELOP = 4,
	STV0900_DISEQC_3_3_ENVELOP = 5
};

enum stv0900_demod_mode {
	STV0900_SINGLE = 0,
	STV0900_DUAL
};

struct stv0900_init {
	/* IQ from the tuner1 to the demod */
	enum stv0900_iq_inversion	tun1_iq_inv;

	/* IQ from the tuner2 to the demod */
	enum stv0900_iq_inversion	tun2_iq_inv;
};

struct stv0900_search {
	s32	frequency;/* Transponder frequency (in KHz) */
	s32	symbol_rate;/* Transponder symbol rate  (in bds)*/
	s32	search_range;/* Range of the search (in Hz) */
	s32 bandwidth;/* Tuner bandwidth in Hz */
	u8	scan_mode;

	enum stv0900_demod_num		path;/* Path Used demod1 or 2 */
	enum stv0900_standard		standard;
	enum stv0900_modulation		modulation;
	enum stv0900_fec			fec;
	enum stv0900_modcode		modcode;
	enum stv0900_search_iq		iq_inversion;
	enum stv0900_algo			search_algo;
};

struct stv0900_signal {
	int	locked;/* Transponder locked */
	u64	frequency;/* Transponder frequency */
	u64	bad_lock_freq;
	s32	symbol_rate;/* Transponder symbol rate */
	s32	cnr;
	s32 signal_level;

	s32 lock_attempts;
	s32	timing_ref;
	s32	lock_time;
	s32	fine_lock_time;
	s32	lock_timeout;
	s32	bad_lock_sr;
	
	s32	coarse_cf_mean;
	s32	coarse_cf_sdev;
	s32	coarse_sr_mean;
	s32	coarse_sr_sdev;
	
	enum fe_status				status;
	enum stv0900_standard		standard;
	enum stv0900_fec			fec;
	enum stv0900_modcode		modcode;
	enum stv0900_modulation		modulation;
	enum stv0900_pilot			pilot;
	enum stv0900_frame_length	frame_len;
	enum stv0900_iq_inversion	spectrum;
	enum stv0900_rolloff		rolloff;
};

/* maximum allowed spectrum points, sufficient for 10 kHz resolution from 950 to 2150 MHz with cal data */
#define SpectrumSize    240002

struct stv0900_spectrum {
	u64 start;
	u64 step;
	s32 nsteps;
	s32 bw;
	s32 *data;
};

struct stv0900_blindscan {
	u32 srch_range;
	s32 min_cf;
	s32 max_cf;
	s32 min_sr;
	s32 max_sr;
	s32 init_sr_bits;
	s32 min_sr_bits;
	s32 max_sr_bits;
	s32 jump;
	u8  coarse_init_done;
	u8  skip_tune;
	u8	center_tune;
	struct stv0900_demod *demod;
};

struct stv0900_chip;

/* state for each demod */
struct stv0900_demod {
	u64		freq;
	u64		srch_range;
	u32		mclk;
	s32		bw;
	s32		bbgain;
	s32		prev_gain;
	s32		symbol_rate;
	s32		demod_timeout;
	s32		fec_timeout;
	u8		index;
	u8		tuner_auto;
	u8		tuner_type;
	u8		scan_mode;
	u8		i2c_addr;
	u8		chip_id;
	u8		init_complete;
	u8		mutex_count;
	
	enum stv0900_algo			algo;
	enum stv0900_standard		standard;
	enum stv0900_search_iq		iq_inv;
	enum stv0900_modcode		modcode;
	enum stv0900_modulation		modulation;
	enum stv0900_fec			fec;
	enum stv0900_rolloff		rolloff;
	enum stv0900_error			err;
	
	struct dvb_frontend			frontend;
	struct stv0900_init			init_params;
	struct stv0900_chip			*chip;
	struct i2c_adapter			*i2c;
	const struct stv0900_config	*config;
	
	struct stv0900_signal		result;
	struct stv0900_blindscan	blind;
	struct stv0900_spectrum		spectrum;
};

/* state for each chip */
struct stv0900_chip {
	u32		quartz;
	u32		mclk;
	u8		dmds_used;
	u8		sleeping[2];
	u8		clkmode;
	u8		i2c_addr;
	u8		chip_id;
	u8		mutex_present;
	
	enum stv0900_demod_mode	demod_mode;
	enum stv0900_error		errs;

	struct mutex			demod_mutex;
	struct i2c_adapter		*i2c;
	struct stv0900_demod	*demod[2];
	struct stv0900_reg		*ts_config;
};

extern int stvdebug;

extern s32 ge2comp(s32 a, s32 width);

extern void stv0900_write_reg(struct stv0900_demod *demod, u16 reg, u8 data);
extern void stv0900_write_regs(struct stv0900_demod *demod, u16 reg, u8 *data, u8 len);
extern void stv0900_write_be(struct stv0900_demod *demod, u16 reg, u32 data, u8 len);
extern void stv0900_write_be64(struct stv0900_demod *demod, u16 reg, u64 data, u8 len);
extern u8   stv0900_read_reg(struct stv0900_demod *demod, u16 reg);
extern void stv0900_read_regs(struct stv0900_demod *demod, u16 reg, u8 *data, u8 len);
extern u32  stv0900_read_be(struct stv0900_demod *demod, u16 reg, u8 len);
extern u64  stv0900_read_be64(struct stv0900_demod *demod, u16 reg, u8 len);
extern void stv0900_write_bits(struct stv0900_demod *demod, u32 label, u8 val);
extern u8   stv0900_get_bits(struct stv0900_demod *demod,u32 label);

extern int  stv0900_get_demod_lock(struct stv0900_demod *demod);
extern int  stv0900_check_signal_presence(struct stv0900_demod *demod);
extern enum stv0900_signal_type stv0900_algo(struct stv0900_demod *demod);

extern void stv0900_set_tuner(struct stv0900_demod *demod, u64 frequency, u32 bandwidth);
extern void stv0900_set_bandwidth(struct stv0900_demod *demod, u32 bandwidth);
extern u32  stv0900_get_tuner_freq(struct stv0900_demod *demod);

extern void stv0900_start_search(struct stv0900_demod *demod);
extern u8   stv0900_get_optim_carr_loop(struct stv0900_demod *demod);
extern u8   stv0900_get_optim_short_carr_loop(struct stv0900_demod *demod);
extern enum stv0900_standard stv0900_get_standard(struct stv0900_demod *demod);
extern void stv0900_stop_all_s2_modcod(struct stv0900_demod *demod, u32 index);
extern void stv0900_activate_s2_modcod(struct stv0900_demod *demod);
extern void stv0900_activate_s2_modcod_single(struct stv0900_demod *demod, u32 index);

extern enum fe_status stv0900_get_status(struct stv0900_demod *demod);
extern int	stv0900_read_status(struct dvb_frontend *fe, enum fe_status *status);
extern s32  stv0900_get_signal_level(struct stv0900_demod *demod);
extern s32  stv0900_get_snr(struct stv0900_demod *demod);
extern s32  stv0900_get_agc2_offset(struct stv0900_demod *demod);
extern s32  stv0900_get_carr_freq(struct stv0900_demod *demod);
extern u32  stv0900_get_symbol_rate(struct stv0900_demod *demod);
extern s32	stv0900_get_timing_offst(struct stv0900_demod *demod, u32 srate);
extern void stv0900_set_symbol_rate(struct stv0900_demod *demod, u32 sr);
extern void stv0900_set_symbol_rate_bounds(struct stv0900_demod *demod, u32 min_sr, u32 init_sr, u32 max_sr);
extern void stv0900_set_symbol_rate_range(struct stv0900_demod *demod, u32 sr, u32 percent);
extern u32  stv0900_get_symbol_rate_priv(struct stv0900_demod *demod);
extern void stv0900_reset_symbol_rates(struct stv0900_demod *demod);

extern int  stv0900_check_early_exit(struct stv0900_demod *demod);
extern void stv0900_mutex_lock(struct stv0900_demod *demod);
extern void stv0900_mutex_unlock(struct stv0900_demod *demod);
extern s64  stv0900_sqrt64(u64 n);
extern s32  stv0900_ln2(u64 n);
extern void stv0900_shellsort(s32 *a, u32 n);

#endif
